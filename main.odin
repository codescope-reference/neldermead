package main


import "core:fmt"
import SDL "vendor:sdl2"
import "core:math"

pow :: math.pow


// WHEIGHT :: 800 
// WWIDTH  :: 800 

// WHEIGHT :: 540 
// WWIDTH  :: 960 

WHEIGHT :: 720 
WWIDTH  :: 1280 

main :: proc() {
    // INIT SDL
    init_result := SDL.Init(SDL.INIT_VIDEO)
    window : ^SDL.Window
    renderer : ^SDL.Renderer
    result := SDL.CreateWindowAndRenderer(WWIDTH, WHEIGHT, SDL.WINDOW_SHOWN, &window, &renderer) 
    SDL.SetRenderDrawBlendMode(renderer, SDL.BlendMode.BLEND)
    // /INIT SDL

    // CLEAR SCREEN
    SDL.SetRenderDrawColor(renderer, 0, 0, 0, 255)
    SDL.RenderClear(renderer)


    // DEFINE FUNCTION TO VISUALIZE
    // test_func := make_himmelblau(200, 200) 
    // func := proc(x: f64, y: f64) -> f64 {
        // return x * x + y * y
    // }
    // test_func := make_fun(200, 200, func, 2.0) 

    func := proc(x: f64, y: f64) -> f64 { 
            return pow(x*x + y - 11, 2) + pow(x + y*y - 7, 2) 
    }
    range := 6.0
    test_func := make_fun(200, 200, func, range)

    // func := proc(x: f64, y: f64) -> f64 {
        // return (x - 5) * (x - 5) + y * y + (x + 5) * (x + 5)
    // }

    // return make_fun(w, h, func, range)

    // test_func := make_fun(200, 200, func, 6.0)

    // test_func := make_2_parabolas(200, 200, 10.0) 
    // test_func := make_parabola(200, 200, 1.0) 
    // test_func := make_fun(200, 200, 
        // proc(x: f64, y: f64) -> f64 {
            // return pow(x-0.5, 2)  + pow(y + x, 2)
        // }
        // 10.0) 
    defer delete_f2d(test_func) 


    // VISUALIZATION SETTINGS
    // Cap all values above this to same color
    // f_cap : Maybe(f64) = 5.0 
    f_cap : Maybe(f64) = 10000.0 
    // Use log scale
    log_scale := true 
    // Alpha for func vis
    alpha : u8 = 100 



    // TESTING
    // visualize_function(test_func, renderer, f_cap, log_scale, 255)
    // draw_triangle(renderer,
    // { 200, 200 },
    // { 200, 250 },
    // { 250, 250 })

    // SDL.RenderDrawLine(renderer, 200, 200, 200, 400)


    // GET IMAGE DATA AND SAVE TO FILE
    // save_frame("test.png", renderer)


    triangle := Triangle {
        p1 = {2.0, -3.0},
        p2 = {2.1, -3.0},
        p3 = {2.0, -3.1},
    }


    is_running := true
    event : SDL.Event
    last_render := -500
    first_frame := true
    frame_number := 87 
    visualize_function(test_func, renderer, f_cap, log_scale, 255)
    for is_running {
        current_frame := cast(int)SDL.GetTicks()
        // Time since last render 
        delta_time := current_frame - last_render

        if (delta_time > 500) {
            visualize_function(test_func, renderer, f_cap, log_scale, alpha)
            last_render = current_frame
            triangle = amoeba_opt(func, triangle)
            draw_triangle(renderer, triangle, range)
            fname := fmt.caprintf("frames/frame_%04v.png", frame_number)
            save_frame(fname, renderer)
            frame_number += 1
        }


        SDL.RenderPresent(renderer)

        for SDL.PollEvent(&event) {
            #partial switch event.type {
                case SDL.EventType.QUIT: {
                    is_running = false
                    break
                }
            }
        }

        // SDL.Delay(500)
    }
}
