package main

import "core:math"
import "core:fmt"


Function2D :: struct {
    data : []f64
    width : int 
    height : int 
}

delete_f2d :: proc(f: ^Function2D) {
    delete(f.data)
    free(f)
}


// Make a function with the given closure and coordinate range
make_fun :: proc(w: int, h: int, f: proc(x: f64, y: f64) -> f64, range : f64 = 1.0) -> ^Function2D {
    data := make([]f64, w * h)

    center_x := w / 2
    center_y := h / 2


    for idx in 0..<w*h {
        x := idx % w
        y := idx / w
        dx := cast(f64)(x - center_x) / cast(f64)w * 2.0 * range
        dy := cast(f64)(y - center_y) / cast(f64)h * 2.0 * range
        assert(dx >= -range && dx <= range)
        assert(dy >= -range && dy <= range)

        data[idx] = f(dx, dy)
    }


    func := new(Function2D)

    func.data = data
    func.width = w
    func.height = h
    

    return func

}


// Basic x^2 + y^2 parabola
make_parabola :: proc(w: int, h: int, range: f64) -> ^Function2D {
    func := proc(x: f64, y: f64) -> f64 { 
            return x * x + y * y 
    }

    return make_fun(w, h, func, range)
}


// Himmelblau's test function (https://en.wikipedia.org/wiki/Himmelblau%27s_function)
make_himmelblau :: proc(w: int, h: int) -> ^Function2D {
    func := proc(x: f64, y: f64) -> f64 { 
            return pow(x*x + y - 11, 2) + pow(x + y*y - 7, 2) 
    }

    return make_fun(w, h, func, 6.0)
}

make_2_parabolas :: proc(w: int, h: int, range: f64) -> ^Function2D {
    func := proc(x: f64, y: f64) -> f64 {
        return (x - 5) * (x - 5) + y * y + (x + 5) * (x + 5)
    }

    return make_fun(w, h, func, range)
}
