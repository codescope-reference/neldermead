package main

import STB "vendor:stb/image"
import SDL "vendor:sdl2"


save_frame :: proc(fname : cstring, renderer: ^SDL.Renderer) {
    // Should be called after rendering and before RenderPresent as per SDL docs
    format := cast(u32) SDL.PixelFormatEnum.RGB24
    pixels := make([^]u8, WWIDTH * WHEIGHT * 3)
    pitch := cast(i32) WWIDTH * 3
    SDL.RenderReadPixels(renderer, nil, format, pixels, pitch) 
    STB.write_png(fname, WWIDTH, WHEIGHT, 3, pixels, WWIDTH * 3)
}
