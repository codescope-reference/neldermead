package main

import "core:math/linalg"

FuncType :: proc(x: f64, y: f64) -> f64

Vector :: distinct[2]f64 

Triangle :: struct {
    p1: Vector,
    p2: Vector,
    p3: Vector,
}

reflection :: proc(p_m: Vector, p_h: Vector, alpha: f64) -> Vector {
    // alpha should be > 0
    return p_m + alpha * (p_m - p_h)
}

expansion :: proc(p_m: Vector, p_r: Vector, beta: f64) -> Vector {
    return p_m + beta * (p_r - p_m)
}

contraction :: proc(p_m: Vector, p_h: Vector, gamma: f64) -> Vector {
    return p_m + gamma * (p_h - p_m)
}

shrinkage :: proc(p_h: Vector, p_sh: Vector, p_l: Vector) -> Triangle {
    return Triangle {
        p1 = p_h,
        p2 = (p_h + p_sh) / 2.0,
        p3 = (p_h + p_l) / 2.0,
    }
}

amoeba_opt :: proc(func: FuncType, triangle: Triangle) -> Triangle {
    using triangle
    y1 := func(p1.x, p1.y)
    y2 := func(p2.x, p2.y)
    y3 := func(p3.x, p3.y)

    // The point with the highest value
    // The point with the second highest value
    // The point with the lowest value
    p_h : Vector = p1
    y_h := y1
    p_sh : Vector = p2
    y_sh := y2 
    p_l : Vector = p3
    y_l := y3

    if (y2 > y_h) {
        p_sh = p_h
        y_sh = y_h

        p_h = p2
        y_h = y2
    }

    if (y3 > y_h) {
        p_l = p_sh
        y_l = y_sh

        p_sh = p_h
        y_sh = y_h

        p_h = p3
        y_h = y3
    } else if (y3 > y_sh) {
        p_l = p_sh
        y_l = y_sh

        p_sh = p3
        y_sh = y3
    }

    p_m := (p_sh + p_l) / 2.0
    p_r := reflection(p_m, p_h, 1.0)
    y_r := func(p_r.x, p_r.y)

    if (y_r < y_l) {
        p_e := expansion(p_m, p_r, 2.0)
        y_e := func(p_e.x, p_e.y)

        if (y_e < y_r) {
            return Triangle {
                p1 = p_e,
                p2 = p_sh,
                p3 = p_l,
            }
        } else {
            return Triangle {
                p1 = p_r,
                p2 = p_sh,
                p3 = p_l,
            }
        }
    } else {
        if (y_r >= y_sh) {
            if (y_r < y_h) {
                p_h = p_r
                y_h = func(p_h.x, p_h.y)
            }

            p_c := contraction(p_m, p_h, 0.5)
            y_c := func(p_c.x, p_c.y)

            if (y_c > y_h) {
                return shrinkage(p_h, p_sh, p_l)
            } else {
                return Triangle {
                    p1 = p_c,
                    p2 = p_sh,
                    p3 = p_l,
                }
            }
        } else {
            return Triangle {
                p1 = p_r,
                p2 = p_sh,
                p3 = p_l,
            }
        }

    }
}





    // if (y1 > y2) {
        // if (y1 > y3) {
            // p_h = p1
            // if (y2 > y3) {
                // p_sh = p2
                // p_l = p3
            // } else {
                // p_sh = p3
                // p_l = p2
            // }
        // } else {
            // p_h = p3
            // p_sh = p1
            // p_l = p2
        // }
    // } else { 
        // if (y2 > y3) {
            // p_h = p2
            // if (y1 > y3) {
                // p_sh = p1
                // p_l = p3
            // } else {
                // p_sh = p3
                // p_l = p1
            // }
        // } else {
            // p_h = p3
            // p_sh = p2
            // p_l = p1
        // }
    // }
