package main

import SDL "vendor:sdl2"
import "core:fmt"

Point :: struct {x: i32, y: i32}
Color :: struct {r: u8, g: u8, b: u8, a: u8}

draw_triangle_pts :: proc(renderer: ^SDL.Renderer, p1: Point, p2: Point, p3: Point,
                        color : Color = { 255, 255, 255, 255 } ) {
    SDL.SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a)
    SDL.RenderDrawLine(renderer, p1.x, WHEIGHT - p1.y, p2.x, WHEIGHT - p2.y)
    SDL.RenderDrawLine(renderer, p2.x, WHEIGHT - p2.y, p3.x, WHEIGHT - p3.y)
    SDL.RenderDrawLine(renderer, p3.x, WHEIGHT - p3.y, p1.x, WHEIGHT - p1.y)
}

draw_triangle :: proc(renderer: ^SDL.Renderer, triangle: Triangle,
                       range: f64, 
                        color : Color = { 255, 255, 255, 255 } ) {

    p1 := Point {
        x = cast(i32) ((triangle.p1.x + range) / (2.0 * range) * WWIDTH)
        y = cast(i32) ((triangle.p1.y + range) / (2.0 * range) * WHEIGHT)
    }
    p2 := Point {
        x = cast(i32) ((triangle.p2.x + range) / (2.0 * range) * WWIDTH)
        y = cast(i32) ((triangle.p2.y + range) / (2.0 * range) * WHEIGHT)
    }
    p3 := Point {
        x = cast(i32) ((triangle.p3.x + range) / (2.0 * range) * WWIDTH)
        y = cast(i32) ((triangle.p3.y + range) / (2.0 * range) * WHEIGHT)
    }
    draw_triangle_pts(renderer,  p1, p2, p3, color)
}
