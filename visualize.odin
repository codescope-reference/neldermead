package main

import "core:math"
import "core:fmt"
import SDL "vendor:sdl2"

visualize_function :: proc(field: ^Function2D,
                           renderer: ^SDL.Renderer,
                           function_cap : Maybe(f64) = nil,
                           log_scale := false,
                           alpha : u8 = 255) {
    f_cap : Maybe(f64) = function_cap 
    if log_scale {
        for value in field.data {
            assert(value >= 0.0)
        }
        val, ok := function_cap.?
        if ok {
            f_cap = math.ln(val)
        }
    }

    min_value := 0.0
    max_value := 0.0
    eps :: 0.01
    for _value, i in field.data {
        value := log_scale ? math.ln(_value + eps) : _value
        if i == 0 {
            min_value = value
            max_value = value
        } else {
            min_value = math.min(value, min_value)
            max_value = math.max(value, max_value)
        }
    }
    max_value = f_cap.? or_else max_value



    for i : i32 = 0; i < WHEIGHT; i += 1 {
        for j : i32 = 0; j < WWIDTH; j += 1 {
            f_j := (cast(i32)field.width * j) / WWIDTH 
            f_i := (cast(i32)field.height * i) / WHEIGHT 

            // Scale field value to go from 0 to 1
            field_value := field.data[f_i * cast(i32)field.width + f_j]
            if log_scale {
                field_value = math.ln(field_value + eps) 
            }
            field_value = math.min(field_value, max_value)

            corr_val := (field_value - min_value) / (max_value - min_value)
            // corr_val = math.sqrt(corr_val)

            blue := cast(u8) (255.0 * (1.0 - corr_val))
            red :=  cast(u8) (255.0 * corr_val * corr_val * 0.0)
            // green :=  cast(u8) (255.0 * corr_val * corr_val)
            green : u8 = cast(u8) (255.0 * math.pow(1.0 - corr_val, 2))
             

            _ = SDL.SetRenderDrawColor(renderer, red, green, blue, alpha)
            _ = SDL.RenderDrawPoint(renderer, j, WHEIGHT - i - 1) 
        }
    }
}

